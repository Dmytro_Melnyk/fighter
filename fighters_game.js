/* Your code goes here */

class Fighter {
  #name;
  #damage;
  #agility;
  #hp;
  #maxHp;
  #wins = 0;
  #losses = 0;

  constructor(name, damage, agility, hp) {
    this.#name = name;
    this.#damage = damage;
    this.#agility = agility;
    this.#hp = hp;
    this.#maxHp = hp;
  }

  getName() {
    return this.#name;
  }

  getDamage() {
    return this.#damage;
  }

  getAgility() {
    return this.#agility;
  }

  getHealth() {
    return this.#hp;
  }

  setName(value) {
    if (!value) {
      throw new Error('Negative water');
    }
    this.#name = value;
  }

  setDamage(value) {
    this.#damage = value;
  }

  setAgility(value) {
    this.#agility = value;
  }

  setHealth(value) {
    if (value < 0) {
      throw new Error('Negative water');
    }
    this.#hp = value;
  }

  attack(fighter) {
    let accuracy = Math.floor(Math.random() * 100 + 1);
    if (accuracy <= (100 - fighter.getAgility())) {

      fighter.dealDamage(this.getDamage());
      console.log(`${this.getName()} make ${this.getDamage()} damage to ${fighter.getName()}`)
    } else {
      console.log(`${this.getName()} attack missed`)
    }
  }

  logCombatHistory() {
    console.log(`Name: ${this.getName()}, Wins: ${this.#wins}, Losses: ${this.#losses}`);
  }

  heal(points) {
    if ((this.getHealth() + points) <= this.#maxHp) {
      this.setHealth(this.getHealth() + points);
    } else {
      this.setHealth(this.#maxHp);
    }
  }

  dealDamage(points) {
    if ((this.getHealth() - points) >= 0) {
      this.setHealth(this.getHealth() - points);
    } else {
      this.setHealth(0);
    }
  }

  addWin() {
    this.#wins++;
  }

  addLoss() {
    this.#losses++;
  }

}

function battle(fighter1, fighter2) {
  if (!fighter1.getHealth()) {
    console.log(`${fighter1.getName()} is dead and can't fight.`);
  } else if (!fighter2.getHealth()) {
    console.log(`${fighter2.getName()} is dead and can't fight.`);
  } else {
    while (fighter1.getHealth() && fighter2.getHealth()) {
      fighter1.attack(fighter2);
      if (fighter2.getHealth()) {
        fighter2.attack(fighter1);
      }
    }
    if (!fighter1.getHealth()) {
      console.log(`${fighter1.getName()} is loser!`);
      console.log(`${fighter2.getName()} is winner!`);
      fighter1.addLoss();
      fighter2.addWin();
    } else if (!fighter2.getHealth()) {
      console.log(`${fighter2.getName()} is loser!`);
      console.log(`${fighter1.getName()} is winner!`);
      fighter2.addLoss();
      fighter1.addWin();
    }
  }
}

